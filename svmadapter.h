#ifndef SVMADAPTER_H
#define SVMADAPTER_H
#include <vector>
#include "svm.h"

class SVMAdapter
{
public:
    SVMAdapter();
    SVMAdapter(int nOfTraingMFCC, int numOfCoef, int nOfDifferentSamples);
    void mfccToSVMInput(const std::string &className, std::vector<double> *coef);
    struct svm_problem* getProblem();

private:
    struct svm_parameter *param;
    struct svm_model *model;
    struct svm_problem *problem;
    void initElements(int nOfTraningMFCC, int numOfCoef, int nOfDifferentSamples);
    std::vector <std::string> *classNames;

    static const double eps=0.000001;
};

#endif // SVMADAPTER_H
