#ifndef MAIN_H
#define MAIN_H
#include <QObject>
#include "sound/soundio.h"
#include <QMediaPlayer>
#include "sound/signaltransformations.h"
#include "svmadapter.h"

class Task : public QObject
{
    Q_OBJECT
public:
    Task(QObject *parent = 0) : QObject(parent) {}

public slots:
    void run()
    {
        io=new SoundIO();
        QObject::connect(io, SIGNAL(recorded()), this, SLOT(endedRecord()));
        QObject::connect(io, SIGNAL(played()), this, SLOT(endedPlaying()));

        io->recordSampleToBuffer(1);
    }
    void endedRecord(){
        io->playRecordFromBuffer();

    }

    void endedPlaying(){
//        MelFilterbank *mf=new MelFilterbank(26,300,8000,48000);
//        std::vector<QByteArray*> *frames=io->getSignalFrames(10,5);
//        SignalTransformations st;
//        qDebug()<<"frames"<<frames->size();
//        for (int i=0;i<frames->size();i++){
//            qDebug()<<frames->at(i)->size();
//            st.calculateMFCC(frames->at(i), mf);


//        }
//        st.calculateMFCC(frames->at(0), mf);
//        io->readSampleFromFile("/Users/marisoozar/bin/test.mp3");
        SVMAdapter adapter;

        std::vector<double> *test=new std::vector<double>(0,0.0f);
        for (int i=0;i<5;i++){
            if (i%2 ==0){
                test->push_back(0.0f);
            }
            else test->push_back(i);
        }
        adapter.mfccToSVMInput("zero", test);
        adapter.getProblem();

        delete io;
//        delete frames;
//        delete mf;

        emit finished();
    }


signals:
    void finished();
private:
    SoundIO *io;
};


#endif // MAIN_H
