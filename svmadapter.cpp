#include "svmadapter.h"
#include <vector>
#include <string>

using namespace std;

SVMAdapter::SVMAdapter()
{
    initElements(10,10,10);
}

SVMAdapter::SVMAdapter(int nOfTraningMFCC, int numOfCoef, int nOfDifferentSamples)
{
    initElements(nOfTraningMFCC, numOfCoef, nOfDifferentSamples);

}

void SVMAdapter::mfccToSVMInput( const std::string &className, std::vector<double> *coef)
{
    int classNum=-1;
    for (int i=0;i<classNames->size();i++){
        if (className.compare(classNames->at(i))==0){
            classNum=i+1;
        }
    }
    if (classNum==-1){
        classNames->push_back(className);
        classNum=classNames->size();
    }


    problem->y[problem->l]=classNum;

//    bool first=true;
    int currCoef=0;
    struct svm_node *ptr=*problem->x;
    for (int i=0;i<coef->size();i++){
        if (coef->at(i)>eps || coef->at(i)<-eps){
            struct svm_node *node=new svm_node();
            node->index=i+1;
            node->value=coef->at(i);

            problem->x[problem->l][currCoef]=*node;
            currCoef++;
        }
    }
    struct svm_node *last=new svm_node();
    last->index=-1;
    problem->x[problem->l][currCoef+1]=*last;

    problem->l++;
}

svm_problem* SVMAdapter::getProblem()
{
    return problem;
}

void SVMAdapter::initElements(int nOfTraingMFCC, int numOfCoef, int nOfDifferentSamples)
{
    problem=new svm_problem();
    param=new svm_parameter();
    classNames=new vector<string>(nOfDifferentSamples);

    param->svm_type=C_SVC;
    param->kernel_type=RBF;
    param->eps=0.001;
    param->cache_size=256;
    param->C=100;
    param->nr_weight=0;
    problem->l=nOfTraingMFCC;
    problem->y=new double[nOfTraingMFCC];
    problem->x=new svm_node*[nOfTraingMFCC];
    for (int i=0;i<nOfTraingMFCC;i++){
        problem->x[i]=new struct svm_node[numOfCoef];
    }

}
