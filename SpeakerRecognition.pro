# Add more folders to ship with the application, here
folder_01.source = qml/SpeakerRecognition
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QMAKE_CXXFLAGS += -Wall

# Additional import path used to resolve QML modules in Creator's code model
#QML_IMPORT_PATH =
QT +=multimedia

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    sound/signaltransformations.cpp \
    sound/melfilterbank.cpp \
    sound/soundio.cpp \
    svm.cpp \
    svmadapter.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    sound/signaltransformations.h \
    sound/melfilterbank.h \
    sound/soundio.h \
    main.h \
    svm.h \
    svmadapter.h
