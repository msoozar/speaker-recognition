#include "signaltransformations.h"
#include "melfilterbank.h"
#include "soundio.h"
#include "QtMultimedia/QAudioDeviceInfo"
#include <QDebug>
using namespace std;

void SignalTransformations::fft(vector<base> &a, int N){
    int n=a.size();
    if (n==1) return;

    vector<base> a0(n/2, base()), a1(n/2, base());
    for (int i=0, j=0;i<n;i+=2, j++){
        a0[j]=a[i];
        a1[j]=a[i+1];
    }

    fft(a0, N);
    fft(a1, N);

    double ang=-2*M_PI/n;
    base w(1), wn(cos(ang), sin(ang));
    for (int i=0;i<n/2;i++){
        a[i]=a0[i] + w*a1[i];
        a[i+n/2]=a0[i] - w*a1[i];
        w*=wn;
    }
}


void SignalTransformations::hammingWindow(vector<double> &input)
{
    int N=input.size();

    for (int n=0;n<N;n++){
        input.at(n)=0.54-0.46*cos((2*M_PI)*input.at(n)/(N-1));
    }
}

vector<double> SignalTransformations::kIndex(int freq, int N)
{
    vector<double> kIndex(N/2);
    for (int i=0;i<N/2;i++){
        kIndex[i]=(double)freq*i/N;
    }
    return kIndex;
}

double SignalTransformations::herzToMel(double freq)
{
    return 1127*log(1+freq/700.0);
}

double SignalTransformations::melToHerz(double mel)
{
    return 700*(exp(mel/1127.0)-1);
}


std::vector<double> SignalTransformations::calculatePowerSpectralEstimates(std::vector<base> &fftResult, int N, int len)
{
    vector<double> power(len,0.0f);
    for(int i=0;i<len;i++){
        power[i]=(1./N)*pow(abs(fftResult.at(i)),2);
    }

    return power;
}

std::vector<double> SignalTransformations::cosineTransform(std::vector<double> *source)
{
    int N=source->size();
    vector<double> res(N);
    for (int k=0;k<N;k++){
        double coef=0;
        for (int n=0; n<N; n++){
            coef+= source->at(n) * cos((M_PI / N)* (n+0.5) * k);
        }
        coef*= (k==0)? sqrt(1./N) : sqrt(2./N);

        res[k]=coef;
    }
    return res;

}

void SignalTransformations::logarithm(std::vector<double> *source)
{
    for (int i=0;i<source->size();i++){
        source->at(i)=log(source->at(i));
    }
}

void SignalTransformations::calculateMFCC(QByteArray *input, MelFilterbank* mf)
{
//    MelFilterbank mf=MelFilterbank(26,300,8000,8000);

    vector<double> signal(input->size(), 0.0f);
    for (int i=0;i<signal.size();i++){
        signal[i]=input->at(i);
    }
//    std::copy(input->begin(),input->end(),signal.begin());

    hammingWindow(signal);

    vector<base> v1(fftLen,base());
    for (int i=0;i<fftLen;i++){
        if (i>signal.size()){
            v1[i]=0;
        }else
        v1[i]=signal[i];
//        printf("%f\n",signal[i]);
    }
    qDebug()<<"before fft";
    fft(v1,fftLen);
    printf("v size: %llu\n",v1.size());
    vector<double> power=calculatePowerSpectralEstimates(v1,fftLen, mf->getNumberOfRequiredCoefficients());
    printf("power size: %llu\n",power.size());
    vector<double> *energy=mf->calculateEnergies(power);
    printf("energy size: %llu\n",energy->size());
    logarithm(energy);
    printf("energy log size: %llu\n",energy->size());
    vector<double> v2=cosineTransform(energy);
    printf("v2 size %llu\n",v2.size());
    for (int i=0;i<v2.size();i++){
        printf("%f\n",v2[i]);
    }
}




enum {
    N_ITEMS = 8,
};

