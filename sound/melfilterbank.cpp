#include "melfilterbank.h"
#include "signaltransformations.h"
#include <cmath>
using namespace std;

MelFilterbank::MelFilterbank()
{
    initializeFileds(26,300,8000, 8000);
}

MelFilterbank::MelFilterbank(int numberOfFilters, int baseFreq, int topFreq, int sampleRate)
{
    initializeFileds(numberOfFilters, baseFreq, topFreq, sampleRate);
}

void MelFilterbank::initializeFileds(int numberOfFilters, int baseFreq, int topFreq, int sampleRate)
{
    this->numberOfFilters=numberOfFilters;
    this->baseFreq=baseFreq;
    this->topFreq=topFreq;
    this->sampleRate=sampleRate;
    //inner boundaries +2 outer
    boundaries.resize(numberOfFilters+2);
    filters.resize(numberOfFilters);

    //excluded 0-coefficient will be replaced by adding 1 coefficient from the end
    requiredCoefficients=(topFreq*512/sampleRate)+1;

    for (int i=0;i<numberOfFilters;i++){
        filters[i].resize(requiredCoefficients);
    }
    calculateFilters();
}

void MelFilterbank::calculateFilters()
{
    double melLowerBound=SignalTransformations::herzToMel(baseFreq);
    double melUpperBound=SignalTransformations::herzToMel(topFreq);
    double interval=(melUpperBound-melLowerBound)/(numberOfFilters+1);
    //number of coefficients, which will be taken from fft

    vector<double> f;
    for (int i=0;i<numberOfFilters+2;i++){
        double bound=SignalTransformations::melToHerz(melLowerBound+i*interval);
        boundaries[i]=bound;

        f.push_back(floor((512*bound)/sampleRate));
    }

    for (int i=0;i<numberOfFilters;i++){
        for (int j=0, k=1;j<requiredCoefficients;j++, k++){
            if (k<f[i] || k>f[i+2]) filters[i][j]=0;
            else if (f[i]<=k && k<=f[i+1]){
                    filters[i][j]=(k-f[i])/(f[i+1]-f[i]);
            } else if (f[i+1]<=k && k<=f[i+2]){
                filters[i][j]=(f[i+2]-(k))/(f[i+2]-f[i+1]);
            }
        }
    }

}

int MelFilterbank::getNumberOfFilters()
{
    return numberOfFilters;
}
int MelFilterbank::getBaseFrequency()
{
    return baseFreq;
}
int MelFilterbank::getTopFrequency()
{
    return topFreq;
}

int MelFilterbank::getNumberOfRequiredCoefficients()
{
    return requiredCoefficients;
}

std::vector<double>* MelFilterbank::calculateEnergies(std::vector<double> &powerSpectrum)
{
    vector<double> *energy=new vector<double>(numberOfFilters,0.0f);
    for (int i=0;i<numberOfFilters;i++){
        double sum=0;
        for (int j=0;j<requiredCoefficients;j++){
            sum+=powerSpectrum[j]*filters[i][j];
        }
        energy->at(i)=sum;

    }
    return energy;
}

