#include "soundio.h"
#include <QMultimedia>
#include <iostream>
#include <QDebug>
#include <QAudioDecoder>
#include <QBuffer>
#include "math.h"


SoundIO::SoundIO()
{
    format.setSampleRate(16000);
    format.setChannelCount(1);
    format.setSampleSize(8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::UnSignedInt);

}

SoundIO::~SoundIO()
{

}

void SoundIO::recordSampleToBuffer(int sec){

    recordFile.setFileName("/Users/marisoozar/bin/test.raw");
    recordFile.open( QIODevice::WriteOnly | QIODevice::Truncate );
    buffer.open(QIODevice::WriteOnly | QIODevice::Truncate);

    QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
    if (!info.isFormatSupported(format)) {
//        qWarning() << "Default format not supported, trying to use the nearest.";
        format = info.nearestFormat(format);
        qDebug()<<format;
    }

    audioInput = new QAudioInput(format,this);
    connect(audioInput, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleStateChanged(QAudio::State)));

    QTimer::singleShot(sec*1000, this, SLOT(stopRecording()));
//    audioInput->start(&recordFile);
    audioInput->start(&buffer);
    // Records audio for 3000ms
}

void SoundIO::readSampleFromFile(std::string filename)
{
    QString qFilename=QString::fromStdString(filename);
//    QFile file(qFilename);
//    file.open(QIODevice::ReadOnly);
    dec=new QAudioDecoder();
    dec->audioFormat();
    dec->setSourceFilename(qFilename);


//    dec->setSourceDevice(file);
    connect(dec, SIGNAL(bufferReady()),this, SLOT(bufferReaded()));
    dec->start();

}

void SoundIO::playRecordFromBuffer()
{
//    recordFile.setFileName("/Users/marisoozar/bin/test.raw");
//    recordFile.open(QIODevice::ReadOnly);
    buffer.open(QIODevice::ReadOnly);


//    QAudioBuffer *buffer=new QAudioBuffer(48000,format,-1);

    QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
    if (!info.isFormatSupported(format)) {
//        qWarning() << "Raw audio format not supported by backend, cannot play audio.";
//        qWarning()<<info.nearestFormat(format);
        format=info.nearestFormat(format);
//        emit played();
//        return;
    }
    qDebug()<<"playing";
    audioOutput = new QAudioOutput(format, this);
    connect(audioOutput, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handlePlayerStateChanged(QAudio::State)));
    audioOutput->start(&buffer);

}

void SoundIO::setAudioBuffer(QAudioBuffer *buffer)
{
    audioBuffer=buffer;

}

std::vector<QByteArray* >* SoundIO::getSignalFrames(int lenMs, int stepMs)
//std::vector<double > SoundIO::getSignalFrames(int lenMs, int stepMs)
{
    //getting data from original buffer
    buffer.open(QIODevice::ReadWrite);
    QByteArray data=buffer.data();

    //2 channels
    int totalFrames=2*ceil(audioBuffer->duration()/(1000.*stepMs));
    qDebug()<<"total "<<totalFrames<<" "<<audioBuffer->frameCount();
    qDebug()<<" "<<data.size();
    int stepSamples=48*stepMs;
    int frameLen=48*lenMs;

    std::vector<QByteArray*> *frames =new std::vector<QByteArray*>();



    qDebug()<<"step and length"<< stepSamples<<" "<<frameLen;
    //left and right channels
    QByteArray left(audioBuffer->frameCount(), (char)0);
    QByteArray right(audioBuffer->frameCount(),(char)0);
    int samplesTotal=audioBuffer->sampleCount();

    qDebug()<<samplesTotal;
    for (int i=0;i<data.size()/2;i++){
        left[i]=data.at(i*2);
        right[i]=data.at(i*2+1);
    }
    qDebug()<<"first left "<<left.size();

    if (((totalFrames-1)*stepSamples+frameLen)>audioBuffer->frameCount()*2){
        int diff=(( (totalFrames-1) * stepSamples + frameLen) ) - 2*audioBuffer->frameCount();
        qDebug()<<"diff"<<diff;
        for (int i=0;i<diff;i++){
            left.push_back((char)0);
            right.push_back((char)0);
        }
    }
    qDebug()<<"left "<<left.size();


    for (int i=0;i<totalFrames;i++){
        QByteArray *temp=new QByteArray(0,' ');
        //400 samples belonging to the same channel
        if (((i/2)*stepSamples+frameLen)>=left.size()){
            qWarning()<<"not enough data "<<i<<" "<<stepSamples<<" "<<left.size();
            qWarning()<<(i/2)*stepSamples+frameLen;
            break;
        }
        if (i%2==0){
//            std::copy(left.begin()+i*stepSamples,left.begin()+i*stepSamples+frameLen, frames[i].begin());
            for (int j=0;j<frameLen;j++){
//                frames[i][j]=left[(i/2)*stepSamples+j];
                temp->append(left[i*stepSamples/2 + j]);
            }
        }else{
//            std::copy(right.begin()+i*stepSamples,right.begin()+i*stepSamples+frameLen, frames[i].begin());
            for (int j=0;j<frameLen;j++){
//                frames[i][j]=right[(i-1)*stepSamples/2 +j];
                temp->append(right[(i-1)*stepSamples/2 +j]);
            }

        }
        frames->push_back(temp);
    }
    qDebug()<<"end";
    return frames;



}



void SoundIO::handleStateChanged(QAudio::State newState)
{
    switch (newState) {
        case QAudio::StoppedState:
            if (audioInput->error() != QAudio::NoError) {
                // Error handling
            } else {

            }
            break;

        case QAudio::ActiveState:
            // Started recording - read from IO device
            std::cout<<"active"<<std::endl;
            break;

        default:
            // ... other cases as appropriate
            break;
    }
}

void SoundIO::handlePlayerStateChanged(QAudio::State newState)
{
    switch (newState) {
            case QAudio::IdleState:
                // Finished playing (no more data)
                audioOutput->stop();
                recordFile.close();
                delete audioOutput;
                qDebug()<<"played";
//                getSignalFrames(25,10);
                emit played();
                break;

            case QAudio::StoppedState:
                // Stopped for other reasons
                if (audioOutput->error() != QAudio::NoError) {
                    // Error handling
                }
                break;

            default:
                // ... other cases as appropriate
                break;
        }
}
void SoundIO::stopRecording()
{
    qDebug()<<"stop";
    audioInput->stop();
    recordFile.close();

    delete audioInput;
    buffer.open(QIODevice::ReadWrite);
    audioBuffer=new QAudioBuffer(buffer.data(),format, -1);
    buffer.close();

    emit recorded();

}

void SoundIO::bufferReaded()
{
    std::cout<<"readed";


}
