#include "audiofileparser.h"
#include <iostream>
#include "QDebug"
#include "QtEndian"

AudioFileParser::AudioFileParser()
{

}


QAudioBuffer* AudioFileParser::parseWAV(std::string filename)
{
//    QString fileName(filename);
    QFile audiofile(QString::fromStdString(filename));
    qDebug()<<audiofile.exists()<<audiofile.fileName();
    if (!audiofile.open(QIODevice::ReadOnly | QIODevice::ReadWrite)){
            std::cout<<"Error opening file"<<std::endl;
    }

    QByteArray unparsedData=audiofile.readAll();

    QByteArray temp=unparsedData.mid(40,4);
    QByteArray formatData=unparsedData.left(44);
    int dataSize=getIntFromByteArray(&temp);
    temp=unparsedData.mid(44,dataSize);
    QAudioBuffer *buff=new QAudioBuffer(temp, *readFormat(&formatData));
    return buff;
}

QAudioFormat* AudioFileParser::readFormat(QByteArray *formatData)
{
    QAudioFormat *format=new QAudioFormat();
    //1 - 4	 "RIFF"	 Marks the file as a riff file. Characters are each 1 byte long.
    qDebug()<<formatData->mid(0,4);
    if (formatData->mid(0,4)!="RIFF" || formatData->mid(8,4)!="WAVE" ||formatData->mid(12,3)!="fmt"){
        qDebug()<<"File is not a proper .wav";
        return NULL;
    }

    //5 - 8	 File size (integer)	 Size of the overall file - 8 bytes, in bytes (32-bit integer). Typically, you'd fill this in after creation.
    QByteArray temp=formatData->mid(4,4);
    qDebug() << getIntFromByteArray(&temp);
    //9 -12	 "WAVE"	 File Type Header. For our purposes, it always equals "WAVE".
    qDebug()<<formatData->mid(8,4);
    //13-16	 "fmt "	 Format chunk marker. Includes trailing null
    qDebug()<<formatData->mid(12,4);
    //17-20	 16	 Length of format data as listed above
    temp=formatData->mid(16,4);
    qDebug()<<getIntFromByteArray(&temp);
    //21-22	 1	 Type of format (1 is PCM) - 2 byte integer
    temp=formatData->mid(20,2);
    qDebug()<<getIntFromByteArray(&temp);
    //23-24	 2	 Number of Channels - 2 byte integer
    temp=formatData->mid(22,2);
    format->setChannelCount(getIntFromByteArray(&temp));
    //25-28	 44100	 Sample Rate - 32 byte integer. Common values are 44100 (CD), 48000 (DAT). Sample Rate = Number of Samples per second, or Hertz.
    qDebug()<<"Sample rate";
    temp=formatData->mid(24,4);
    format->setSampleRate(getIntFromByteArray(&temp));
    //29-32	 176400	 (Sample Rate * BitsPerSample * Channels) / 8.
    temp=formatData->mid(28,4);
    qDebug()<<getIntFromByteArray(&temp);
    //33-34	 4	 (BitsPerSample * Channels) / 8.1 - 8 bit mono2 - 8 bit stereo/16 bit mono4 - 16 bit stereo
    temp=formatData->mid(32,2);
    qDebug()<<getIntFromByteArray(&temp);
    //35-36	 16	 Bits per sample
    temp=formatData->mid(34,2);
    format->setSampleSize(getIntFromByteArray(&temp));
    //37-40	 "data"	 "data" chunk header. Marks the beginning of the data section.
    qDebug()<<formatData->mid(36,4);
    //41-44	 File size (data)	 Size of the data section.
    temp=formatData->mid(40,4);
    int dataSize=getIntFromByteArray(&temp);
    temp=formatData->mid(44,dataSize);
    return format;
}

int AudioFileParser ::getIntFromByteArray(QByteArray *ref){
    int size=ref->size();
    int res=0;
    for (int i=0;i<size;i++){
        res|=(uchar(ref->at(i)))<<(i*8);
    }
    return res;
}
