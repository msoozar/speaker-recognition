#ifndef AUDIOFILEPARSER_H
#define AUDIOFILEPARSER_H
#include "QtMultimedia"

/**
 * @brief The AudioFileParser class for parsing audio in formats to QAudioBuffer
 */
class AudioFileParser
{
public:
    AudioFileParser();
    QAudioBuffer* parseWAV(std::string filename);
    QAudioFormat* readFormat(QByteArray *formatData);

private:
    int getIntFromByteArray(QByteArray* ref);
};

#endif // AUDIOFILEPARSER_H
