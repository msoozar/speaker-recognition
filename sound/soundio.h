#ifndef SOUNDIO_H
#define SOUNDIO_H
#include <QAudioInput>
#include <QFile>
#include <QTimer>
#include <QObject>
#include  <QAudioOutput>
#include <QAudioBuffer>
#include <QBuffer>
#include <QAudioDecoder>

class SoundIO : public QObject {
    Q_OBJECT
public:
    SoundIO();
    virtual ~SoundIO();
    void recordSampleToBuffer(int sec);
    void readSampleFromFile(std::string filename);
    void playRecordFromBuffer();
    void setAudioBuffer(QAudioBuffer *buffer);
    std::vector< QByteArray* >*  getSignalFrames(int lenMs, int stepMs);
public slots:
    void handleStateChanged(QAudio::State newState);
    void handlePlayerStateChanged(QAudio::State newState);
    void stopRecording();
    void bufferReaded();

signals:
    void recorded();
    void played();

private:
    QFile recordFile;   // Class member
    QAudioInput* audioInput;
    QAudioOutput* audioOutput;
    QAudioFormat format;
    QAudioBuffer* audioBuffer;
    QBuffer buffer;
    QAudioDecoder* dec;

};

#endif // SOUNDIO_H
