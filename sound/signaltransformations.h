#ifndef FFT_H
#define FFT_H
#define _USE_MATH_DEFINES
#include <vector>
#include <math.h>
#include <complex>
#include <iostream>
#include <QByteArray>
#include "melfilterbank.h"

typedef std::complex<double> base;

class SignalTransformations
{
public:
    void fft(std::vector<base>&, int);
    void hammingWindow(std::vector<double> &input);
    std::vector<double> kIndex(int freq, int N);
    static double herzToMel(double freq);
    static double melToHerz(double mel);
    void weightedMel(std::vector<base>&, std::vector<double>);
    void establishBase(int fLow, int fHigh);
    std::vector<double> calculatePowerSpectralEstimates(std::vector<base>& fftResult, int N, int len);
    std::vector<double> cosineTransform(std::vector<double> *source);
    void logarithm(std::vector<double> *source);
    void calculateMFCC(QByteArray *input, MelFilterbank* mf);

    static const int fftLen=512;


};

#endif // FFT_H
