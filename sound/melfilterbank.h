#ifndef MELFILTERBANK_H
#define MELFILTERBANK_H
#include <vector>

class MelFilterbank
{
public:
    MelFilterbank();
    MelFilterbank(int numberOfFilters, int baseFreq, int topFreq, int sampleRate);
    void calculateFilters();
    int getNumberOfFilters();
    int getBaseFrequency();
    int getTopFrequency();
    int getSampleRate();
    int getNumberOfRequiredCoefficients();
    std::vector< std::vector<double> > filters;
    std::vector<double>* calculateEnergies(std::vector<double> &powerSpectrum);
private:
    int numberOfFilters;
    int baseFreq;
    int topFreq;
    int sampleRate;
    int requiredCoefficients;
    std::vector<double> boundaries;


    void initializeFileds(int numberOfFilters, int baseFreq, int topFreq, int sampleRate);

};

#endif // MELFILTERBANK_H
