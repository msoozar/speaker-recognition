#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"
#include "sound/melfilterbank.h"
#include "sound/signaltransformations.h"
#include "sound/soundio.h"
#include "main.h"



int main(int argc, char *argv[])
{
//    QGuiApplication app(argc, argv);

//    QtQuick2ApplicationViewer viewer;
//    viewer.setMainQmlFile(QStringLiteral("qml/SpeakerRecognition/main.qml"));
//    viewer.showExpanded();

//    return app.exec();

    //    SignalTransformations dt;
    //    MelFilterbank mf=MelFilterbank(26,300,8000,8000);
    //    vector<double> v(512);
    //    for (int i=0;i<512;i++){
    //        v[i]=i+1;
    //    }
    //    dt.hammingWindow(v);
    //    vector<base> v1(512);
    //    for (int i=0;i<512;i++){
    //        v1[i]=v[i];
    //    }

    //    printf("init size: %llu\n",v.size());
    //    dt.fft(v1,512);
    //    printf("v size: %llu\n",v1.size());
    //    vector<double> power=dt.calculatePowerSpectralEstimates(v1,512);
    //    printf("power size: %llu\n",power.size());
    //    vector<double> energy=mf.calculateEnergies(power);
    //    printf("energy size: %llu\n",energy.size());
    //    dt.logarithm(energy);
    //    printf("energy log size: %llu\n",energy.size());
    //    vector<double> v2=dt.cosineTransform(energy);
    //    printf("v2 size %llu\n",v2.size());
    //    for (int i=0;i<v2.size();i++){
    //        printf("%f\n",v2[i]);
    //    }
    //    cout<<(QAudioDeviceInfo::defaultInputDevice()).deviceName().toStdString()<<endl;
    QCoreApplication q(argc, argv);

    Task *task = new Task(&q);


        // This will cause the application to exit when
        // the task signals finished.
        QObject::connect(task, SIGNAL(finished()), &q, SLOT(quit()));

        // This will run the task from the application event loop.
        QTimer::singleShot(0, task, SLOT(run()));
//        task->run();
    return q.exec();

}
